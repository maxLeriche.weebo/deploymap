﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net; // pour les outils réseau
using System.Net.Sockets; // pour les sockets
using System.Threading;

namespace deploymap
{
    class Program
    {
        public static byte[] TransformStringToByte(string swap)
        {
            byte[] data = new byte[1024];
            data = Encoding.ASCII.GetBytes(swap);
            return data;
        }

        public static void Main(string[] args)
        {
            Socket sock;
            //IPAddress ip = IPAddress.Parse ("51.91.120.237");
            IPAddress ip = IPAddress.Parse("127.0.0.1");
            IPEndPoint iep = new IPEndPoint(ip, 1212);
            //IPEndPoint iep = new IPEndPoint (IPAddress.Any, 1212);

            Console.WriteLine("La bataille du café : lancement du serveur");

            try
            {
                sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                Console.WriteLine("Socket créée");

                sock.Bind(iep);
                Console.WriteLine("Socket attachée au port 1212");

                sock.Listen(20);
                Console.WriteLine("Serveur ouvert");

                while (true)
                {

                    Socket client = sock.Accept();
                    Console.WriteLine("{0} : joueur {1} accepte...", DateTime.Now, client.RemoteEndPoint);
                    ThreadPool.QueueUserWorkItem(DialogueJoueur, client);
                    
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return;
            }
        }

        private static void DialogueJoueur(object state)
        {
            var rand = new Random();
            Socket client = (Socket)state;
            byte[] retourner;
            if(rand.Next(0,2)==0)
            {
                retourner = TransformStringToByte("3:9:71:69:65:65:65:65:65:73|2:8:3:9:70:68:64:64:64:72|6:12:2:8:3:9:70:68:64:72|11:11:6:12:6:12:3:9:70:76|10:10:11:11:67:73:6:12:3:9|14:14:10:10:70:76:7:13:6:12|3:9:14:14:11:7:13:3:9:75|2:8:7:13:14:3:9:6:12:78|6:12:3:1:9:6:12:35:33:41|71:77:6:4:12:39:37:36:36:44|");
            }
            else
            {
                retourner = TransformStringToByte("67:69:69:69:69:69:69:69:69:73|74:3:9:7:5:13:3:1:9:74|74:2:8:7:5:13:6:4:12:74|74:6:12:7:9:7:13:3:9:74|74:3:9:11:6:13:7:4:8:74|74:6:12:6:13:11:3:13:14:74|74:7:13:7:13:10:10:3:9:74|74:3:9:11:7:12:14:2:8:74|74:6:12:6:13:7:13:6:12:74|70:69:69:69:69:69:69:69:69:76|");
            }
            client.Send(retourner);
        }
    }
}
